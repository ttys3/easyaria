#!/bin/sh

#fedora
#sudo dnf install -y webkit2gtk3-devel

version=`git log --date=iso --pretty=format:"%cd @%h" -1`
if [ $? -ne 0 ]; then
    version="unknown version"
fi

compile=`date +"%F %T %z"`" by "`go version`
if [ $? -ne 0 ]; then
    compile="unknown datetime"
fi

describe=`git describe --tags 2>/dev/null`
if [ $? -eq 0 ]; then
    version="${version} @${describe}"
fi

VERSION_FLAGS="-X 'main.Version=$version' -X 'main.Compile=$compile'"

cp -v ./js/configuration.js ./AriaNg/

#local install
case `uname -s` in
Linux)
	APP_DIR=EasyAria.Linux.x86_64
	rm -f ./${APP_DIR}/easyAria
	go build -ldflags "-s -w $VERSION_FLAGS" -o ./${APP_DIR}/easyAria .
	cp -v tracker.txt ./${APP_DIR}/
	#package
	PKG_FILENAME=`date +%Y%m%d`-${APP_DIR}.zip
	zip -u -r -v ${PKG_FILENAME} ./${APP_DIR}
	#install
	cp -av ${APP_DIR} $HOME/Apps/
	cp -v ${APP_DIR}/EasyAria.desktop $HOME/.local/share/applications/
	APP_INSTALL_DIR=$HOME/Apps
	[ -d $APP_INSTALL_DIR ] || mkdir $APP_INSTALL_DIR
	sed -i "s@/home/hacklog/Apps@${APP_INSTALL_DIR}@g" $HOME/.local/share/applications/EasyAria.desktop
	echo "Linux: install done"
  ;;
Darwin)
	APP_DIR=EasyAria.app
	rm -f ./${APP_DIR}/Contents/MacOS/easyAria
	go build -ldflags "-s -w $VERSION_FLAGS" -o ./${APP_DIR}/Contents/MacOS/easyAria .
	cp -v tracker.txt ./${APP_DIR}/Contents/MacOS/
	#package
	PKG_FILENAME=`date +%Y%m%d`-${APP_DIR}.zip
	zip -u -r -v ${PKG_FILENAME} ./${APP_DIR}
	#install
	cp -av ./${APP_DIR} /Applications/
	echo "Mac: install done"
  ;;
MINGW64_NT*)
  	APP_DIR=./EasyAria.Win.x86_64
  	rm -f ./easyAria.syso
  	rm -f ${APP_DIR}/easyAria.exe
	windres -i ./windows/easyAria.rc -O coff -o ./easyAria.syso
  	go build -ldflags="-H windowsgui -s -w $VERSION_FLAGS" -o ${APP_DIR}/easyAria.exe
  	cp -v tracker.txt ./${APP_DIR}/
	cp -av ${APP_DIR} "/d/Program Files/"
	echo "Windows: install done"
  ;;
*)
  echo "could not detect the kernel ..."
  ;;
esac

ls -lhp 
