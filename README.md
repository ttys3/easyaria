# easyAria

aria2 GUI app for Linux/Mac OS X/Windows. based on AriaNg webui.

# download 

https://github.com/ihacklog/easyAria/releases

# browser integration
[Aria2c Integration](https://chrome.google.com/webstore/detail/edcakfpjaobkpdfpicldlccdffkhpbfk) is recommanded.

## Linux build aria2c
```shell
wget https://github.com/aria2/aria2/releases/download/release-1.34.0/aria2-1.34.0.tar.xz
tar xvJf aria2-1.34.0.tar.xz
cd aria2-1.34.0
#patch https://github.com/hguandl/aria2-patch/blob/master/aria2-fast.patch
wget https://github.com/hguandl/aria2-patch/raw/master/aria2-fast.patch
patch -p1 < aria2-fast.patch

#ref to https://git.archlinux.org/svntogit/community.git/tree/trunk/PKGBUILD?h=packages/aria2
./configure --prefix=/usr/local --with-ca-bundle=/etc/ssl/certs/ca-certificates.crt \
--without-gnutls \
--with-openssl \
--with-sqlite3 \
--with-libcares \
--enable-bittorrent \
--enable-ssl \
--with-disk-cache=64

make -j14
sudo cp  src/aria2c /usr/local/bin/
sudo ln -s /usr/local/bin/aria2c /usr/bin/aria2c
ln -s /usr/local/bin/aria2c /usr/bin/aria2c
```

## Mac OS X guide

you need install aria2 first:

```shell
brew install aria2
```

then download the app.
