#!/bin/sh

APP_INSTALL_DIR=$HOME/Apps
[ -d $APP_INSTALL_DIR ] || mkdir $APP_INSTALL_DIR
cp -v ./EasyAria.desktop $HOME/.local/share/applications/EasyAria.desktop
sed -i "s@/home/hacklog/Apps@${APP_INSTALL_DIR}@g" $HOME/.local/share/applications/EasyAria.desktop
echo "desktop file installation done."
