#!/bin/sh
#author: HuangYeWuDeng
#date: 20180812
# thanks https://github.com/oilervoss/transmission/blob/master/addtracker.sh
# also thanks https://github.com/ngosang/trackerslist

LIVE_TRACKERS_LIST_CMD='wget -q -O- https://raw.githubusercontent.com/ngosang/trackerslist/master/trackers_best_ip.txt'
TRACKER_LIST=`$LIVE_TRACKERS_LIST_CMD  | awk NF | tr '\n' ',' | sed 's/,$/\n/'`

if [ $? -ne 0 ] || [ -z "$TRACKER_LIST" ]; then
    TRACKER_LIST="udp://tracker.coppersurfer.tk:6969/announce,udp://tracker.open-internet.nl:6969/announce,udp://exodus.desync.com:6969/announce,udp://tracker.opentrackr.org:1337/announce,udp://tracker.internetwarriors.net:1337/announce,udp://9.rarbg.to:2710/announce,udp://public.popcorn-tracker.org:6969/announce,udp://tracker.vanitycore.co:6969/announce,udp://mgtracker.org:6969/announce,udp://tracker.tiny-vps.com:6969/announce,udp://tracker.cypherpunks.ru:6969/announce,udp://tracker.torrent.eu.org:451/announce,udp://thetracker.org:80/announce,udp://open.stealth.si:80/announce,udp://bt.xxx-tracker.com:2710/announce,udp://torr.ws:2710/announce,udp://retracker.lanta-net.ru:2710/announce,http://tracker.city9x.com:2710/announce,http://retracker.telecom.by:80/announce,http://t.nyaatracker.com:80/announce"
fi

echo $TRACKER_LIST
