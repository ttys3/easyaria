// +build !debug
package main

import (
	"context"
	"embed"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	_ "net/http/pprof"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strconv"
	"syscall"
	"time"

	"github.com/webview/webview"
	"github.com/zyxar/argo/rpc"
)

var (
	Version = ""
	Compile = ""
)

type Aria2Arch struct {
	Path, InstallCmd string
}

var (
	Aria2Bin  = make(map[string]Aria2Arch)
	Aria2Path Aria2Arch
)

var ExePath string

const (
	windowWidth    = 1024
	windowHeight   = 768
	ariaListenPort = "6801"
	ariaUserAgent  = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.59 Safari/537.36 115Browser/8.4.0"
)

func init() {
	Aria2Bin["darwin"] = Aria2Arch{Path: "/usr/local/bin/aria2c", InstallCmd: "brew install aria2"}
	Aria2Bin["linux"] = Aria2Arch{Path: "/usr/bin/aria2c", InstallCmd: "sudo pacman -S aria2c"}
	//@TODO windows add auto upgrade feature
	//https://github.com/aria2/aria2/releases/latest
	//Location: https://github.com/aria2/aria2/releases/tag/release-1.34.0
	//https://github.com/aria2/aria2/releases/download/release-1.34.0/aria2-1.34.0-win-64bit-build1.zip
	ex, err := os.Executable()
	if err != nil {
		panic(err)
	}
	ExePath = filepath.Dir(ex)
	Aria2Bin["windows"] = Aria2Arch{Path: ExePath + string(filepath.Separator) + "aria2c.exe", InstallCmd: "download on https://aria2.github.io/"}
	Aria2Path = Aria2Bin[runtime.GOOS]
}

var (
	isAriaPrevStarted bool = false
	isAriaNotExists   bool = false
)

func copyAndCapture(w io.Writer, r io.Reader) ([]byte, error) {
	var out []byte
	buf := make([]byte, 1024, 1024)
	for {
		n, err := r.Read(buf[:])
		if n > 0 {
			d := buf[:n]
			out = append(out, d...)
			_, err := w.Write(d)
			if err != nil {
				return out, err
			}
		}
		if err != nil {
			// Read returns io.EOF at the end of file, which is not an error for us
			if err == io.EOF {
				err = nil
			}
			return out, err
		}
	}
	// never reached
	panic(true)
	return nil, nil
}

func UserHomeDir() string {
	if runtime.GOOS == "windows" {
		home := os.Getenv("HOMEDRIVE") + os.Getenv("HOMEPATH")
		if home == "" {
			home = os.Getenv("USERPROFILE")
		}
		return home
	}
	return os.Getenv("HOME")
}

func isAria2Started() bool {
	timeout := time.Millisecond * 100
	conn, _ := net.DialTimeout("tcp", net.JoinHostPort("127.0.0.1", ariaListenPort), timeout)
	if conn != nil {
		conn.Close()
		return true
	}
	return false
}

func startAria2Rpc(done <-chan struct{}) error {
	if isAria2Started() {
		isAriaPrevStarted = true

		go func() {
			for {
				select {
				case <-done:
					log.Printf("done chan recv.\n")
					return
				default:
					time.Sleep(time.Millisecond * 10)
				}
			}
		}()

		return nil
	}

	var cookiePath string
	var userAgent string
	var logLevel string
	flag.StringVar(&cookiePath, "cookie", "", "path to cookie file, support Firefox3 format (SQLite3), "+
		"Chromium/Google Chrome (SQLite3) and the Mozilla/Firefox(1.x/2.x)/Netscape format.")

	flag.StringVar(&userAgent, "ua", "", "custom user agent")
	flag.StringVar(&logLevel, "loglevel", "error", "aria2c log level, either debug, info, notice, warn or error")

	flag.Parse()

	// check bin exists mac: "/usr/local/bin/aria2c"
	aria2cPath := Aria2Path.Path
	if _, err := os.Stat(aria2cPath); os.IsNotExist(err) {
		isAriaNotExists = true
		return errors.New(aria2cPath + " does not exists!\n please install it with :\n " + Aria2Path.InstallCmd)
	}

	downloadDir := UserHomeDir() + string(filepath.Separator) + "Downloads"
	confDir := UserHomeDir() + string(filepath.Separator) + ".easyAria"
	sessionFilepath := confDir + string(filepath.Separator) + "aria2.sess"

	if _, err := os.Stat(downloadDir); os.IsNotExist(err) {
		os.Mkdir(downloadDir, os.ModePerm)
	}
	if _, err := os.Stat(confDir); os.IsNotExist(err) {
		os.Mkdir(confDir, os.ModePerm)
	}
	// https://stackoverflow.com/questions/35558787/create-an-empty-text-file/35558965
	f, _ := os.OpenFile(sessionFilepath, os.O_RDONLY|os.O_CREATE, 0666)
	f.Close()

	/*
	   --load-cookies=<FILE>
	   		Load Cookies from FILE using the Firefox3 format (SQLite3),
	   		Chromium/Google Chrome (SQLite3) and the Mozilla/Firefox(1.x/2.x)/Netscape format.

	   		Note
	   		If aria2 is built without libsqlite3,
	   		then it doesn’t support Firefox3 and Chromium/Google Chrome cookie format.
	*/
	aria2Params := []string{
		"--allow-overwrite=false",
		"--force-save=false",
		"-c",
		"--file-allocation=none",
		fmt.Sprintf("--log-level=%s", logLevel), // debug, info, notice, warn or error
		fmt.Sprintf("--console-log-level=%s", logLevel),
		"--max-tries=0",          // unlimited number of tries
		"--max-file-not-found=0", // default 0
		"--retry-wait=30",        // seconds to wait between retries
		"--max-concurrent-downloads=10",
		"--split=16",          // Download a file using N connections
		"-x16",                //--max-connection-per-server
		"--min-split-size=4M", //--min-split-size
		"--disk-cache=128M",
		"--no-conf",
		"--remote-time=false", //--remote-time
		"--summary-interval=0",
		"--timeout=600", // timeout must be between 1 and 600
		"--check-certificate=false",
		"--enable-rpc=true",
		"--rpc-allow-origin-all=true",
		"--rpc-listen-port=" + ariaListenPort,
		//"--load-cookies=/home/hacklog/Videos/cookies.txt",
		"-d " + downloadDir,
		"--input-file=" + sessionFilepath,
		"--save-session=" + sessionFilepath,
		"--save-session-interval=30",
		fmt.Sprintf("--stop-with-process=%d", os.Getpid()),
		"--rpc-secret=123456",
		fmt.Sprintf("--bt-tracker='%s'", GetBtTracker()),
	}

	if cookiePath != "" {
		aria2Params = append(aria2Params, fmt.Sprintf("--load-cookies=%s", cookiePath))
		log.Printf("load cookies from file: %s", cookiePath)
	}

	if userAgent != "" {
		log.Printf("set user agent to: %s", userAgent)
	} else {
		userAgent = ariaUserAgent
		log.Printf("use default user agent : %s", userAgent)
	}

	aria2Params = append(aria2Params, fmt.Sprintf("--user-agent='%s'", userAgent))

	/*	ctx, cancel := context.WithCancel(context.Background())
		cmd := exec.CommandContext(ctx, aria2cPath, aria2Params...)
	*/
	cmd := exec.Command(aria2cPath, aria2Params...)
	prepareBackgroundCommand(cmd)

	var stdout, stderr []byte
	var errStdout, errStderr error
	// stdoutIn, _ := cmd.StdoutPipe()
	stderrIn, _ := cmd.StderrPipe()
	cmd.Start()

	//go func() {
	//	stdout, errStdout = copyAndCapture(os.Stdout, stdoutIn)
	//}()

	go func() {
		stderr, errStderr = copyAndCapture(os.Stderr, stderrIn)
	}()

	if errStdout != nil || errStderr != nil {
		log.Fatalf("failed to capture stdout or stderr\n")
	}
	outStr, errStr := string(stdout), string(stderr)
	if len(outStr) > 0 || len(outStr) > 0 {
		fmt.Printf("\nout:\n%s\nerr:\n%s\n", outStr, errStr)
	}

	// Abort the walk if done is closed.
	go func() {
		for {
			select {
			case <-done:
				// log.Println("aria2c recv done ch")
				if cmd.ProcessState != nil && !cmd.ProcessState.Exited() {
					log.Println("aria2c recv done ch: not exited")
					cmd.Process.Signal(syscall.SIGTERM)
				} else {
					log.Println("cmd.ProcessState is nil")
				}
				//
				//log.Println("aria2c terminate failed, try kill sig")
				//cmd.Process.Signal(os.Kill)
				//log.Println("kill sig send to aria2c")
			default:
				time.Sleep(time.Millisecond * 10)
			}
		}
	}()

	err := cmd.Wait()
	if err != nil {
		log.Printf("cmd.Run() failed with %s\n", err)
	} else {
		log.Printf("cmd.Run() exit ok\n")
	}
	return nil
}

// static holds our static web server content.
//go:embed AriaNg/*
var static embed.FS

func startServer() string {
	ln, err := net.Listen("tcp", "127.0.0.1:0")
	if err != nil {
		log.Fatal(err)
	}
	go func() {
		defer ln.Close()

		http.Handle("/AriaNg/", http.FileServer(http.FS(static)))
		http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Location", "/AriaNg/")
			w.WriteHeader(http.StatusFound)
			w.Write([]byte("redirec to /AriaNg/"))
		})
		log.Fatal(http.Serve(ln, http.DefaultServeMux))
	}()
	log.Printf("listen addr: %s", "http://"+ln.Addr().String())
	return "http://" + ln.Addr().String()
}

func main() {
	HandleVersion()

	done := make(chan struct{})
	go startAria2Rpc(done)

	url := startServer()

	debug := true
	w := webview.New(debug)
	w.SetTitle(fmt.Sprintf("Easy Aria %s, aria2 ver %s", Version, GetAria2Version()))
	w.SetSize(windowWidth, windowHeight, webview.HintMin)
	w.Navigate(url)

	w.Dispatch(func() {
		log.Printf("url: %s\n", url)
		// localStorage.getItem('AriaNg.Options')
		// w.Eval("setTimeout(function() {window.location.reload();}, 800);")
		if isAriaPrevStarted {
			// webview.Dialog has been removed. But it is likely to be brought back as a standalone module.
			// w.Dialog(webview.DialogTypeAlert, 0, "Warning", "aria2c has been started by previous EasyAria or other program.")
			panic("aria2c has been started by previous EasyAria or other program.")
		}
		if isAriaNotExists {
			// w.Dialog(webview.DialogTypeAlert, 0, "Warning", "aria2 does not exists!\n please install it with command:\n brew install aria2")
			panic("aria2 does not exists!\n please install it with command:\n brew install aria2")
		}
	})

	ctx, cancel := context.WithCancel(context.Background())
	go handleLowSpeed(ctx)

	defer func() {
		w.Destroy()
		log.Printf("defer before cancel")
		cancel()
		log.Printf("defer done cancel")
		time.Sleep(time.Millisecond * 300)
		log.Printf("defer before Destroy")
	}()
	w.Run()
	log.Printf("start send msg to done")
	done <- struct{}{}
}

func handleLowSpeed(ctx context.Context) {
	var err error
	var rpcc rpc.Client
	rpcc, err = rpc.New(context.Background(), "http://localhost:6801/jsonrpc", "123456", time.Second, nil)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return
	}
	defer rpcc.Close()

	// startup warming up
	time.Sleep(time.Second * 6)
	log.Printf("start handleLowSpeed")
	stats := make(map[string]int)
	for {
		select {
		case <-ctx.Done():
			return
		default:
			if infos, err := rpcc.TellActive(); err != nil {
				fmt.Fprintln(os.Stderr, err)
				continue
			} else {
				for _, info := range infos {
					// fmt.Fprintf(os.Stderr, "info: %#v", info)
					spd, _ := strconv.Atoi(info.DownloadSpeed)

					conns, _ := strconv.Atoi(info.Connections)
					psize, _ := strconv.Atoi(info.PieceLength)

					tt, _ := strconv.Atoi(info.TotalLength)
					cl, _ := strconv.Atoi(info.CompletedLength)
					leftPnum := (tt - cl) / psize

					isBigFile := tt > 1024*50
					// total pieces
					pnum, _ := strconv.Atoi(info.NumPieces)
					tooSlow := (leftPnum > 16/2) && (float64(spd) < float64(psize)*(float64(conns)/16))
					zeroSpeed := info.DownloadSpeed == "0" || spd < 1024*10*conns

					if pnum == 0 || leftPnum == 0 {
						continue
					}

					// log.Printf("[%s] spd: %.02f M/s, low spd threhold: %.02f M/s, psize: %d, conns: %d, left/pnum: %d/%d\n",
					// 	info.Files[0].Path,
					// 	float64(spd)/1024/1024, float64(psize)*float64(conns)/16/1024/1024, psize, conns, leftPnum, pnum)

					// if speed < 10 * 1024 bytes/sec or is zero
					if info.Status == "active" && isBigFile && (conns == 0 || zeroSpeed || tooSlow) {
						stats[info.Gid]++
					} else {
						// reset
						stats[info.Gid] = 0
					}
					if slowCnt, ok := stats[info.Gid]; ok && slowCnt > 30 {
						// reset
						stats[info.Gid] = 0
						log.Printf("[%s] low speed[%.02f]M/s auto pause and unpause\n", info.Files[0].Path, float64(spd)/1024/1024)
						if gid, err := rpcc.Pause(info.Gid); err != nil {
							log.Printf("[%s]set to pause failed: %s\n", info.Gid, err)
						} else {
							time.Sleep(time.Millisecond * 100)
							for i := 0; i < 3; i++ {
								if _, err := rpcc.Unpause(gid); err != nil {
									log.Printf("[%s]set pause to unpause failed: %s\n", gid, err)
									time.Sleep(time.Millisecond * 500)
									continue
								} else {
									break
								}
							}
						}
					}
				}
			}

			//if infos, err := rpcc.TellWaiting(0, 10); err != nil {
			//	fmt.Fprintln(os.Stderr, err)
			//	continue
			//} else {
			//	for _, info := range infos {
			//		if info.Status != "paused" {
			//			continue
			//		}
			//		//fmt.Fprintf(os.Stderr, "info: %#v", info)
			//		if _, err := rpcc.Unpause(info.Gid); err != nil {
			//			log.Printf("[%s]set waiting to unpause failed: %s\n", info.Files[0].Path, err)
			//		} else {
			//			log.Printf("[%s]set waiting to unpause success\n", info.Files[0].Path)
			//		}
			//	}
			//}
			time.Sleep(time.Millisecond * 1000)
		}
	}
}
