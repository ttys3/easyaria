package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
)

func GetAria2Version() string {
	//aria2c --version | head -n1 | cut -d' ' -f 3
	cmd := exec.Command("/usr/bin/sh", "-c", fmt.Sprintf("%s --version | head -n1 | cut -d' ' -f 3", Aria2Path.Path))
	out,err := cmd.CombinedOutput()
	//fmt.Printf("out: %#v, err:%#v, %#v", string(out), err, cmd.Args)
	if err == nil {
		return strings.TrimSpace(string(out))
	}
	return ""
}

func HandleVersion() {
	args := os.Args
	if len(args) == 2 && (args[1] == "--version" || args[1] == "-v") {
		fmt.Printf("Version: %s\n", Version)
		fmt.Printf("Compile : %s\n", Compile)
		os.Exit(0)
	}
}

func GetBtTracker() string {
	dft := "udp://tracker.coppersurfer.tk:6969/announce,udp://tracker.open-internet.nl:6969/announce,udp://exodus.desync.com:6969/announce,udp://tracker.opentrackr.org:1337/announce,udp://tracker.internetwarriors.net:1337/announce,udp://9.rarbg.to:2710/announce,udp://public.popcorn-tracker.org:6969/announce,udp://tracker.vanitycore.co:6969/announce,udp://mgtracker.org:6969/announce,udp://tracker.tiny-vps.com:6969/announce,udp://tracker.cypherpunks.ru:6969/announce,udp://tracker.torrent.eu.org:451/announce,udp://thetracker.org:80/announce,udp://open.stealth.si:80/announce,udp://bt.xxx-tracker.com:2710/announce,udp://torr.ws:2710/announce,udp://retracker.lanta-net.ru:2710/announce,http://tracker.city9x.com:2710/announce,http://retracker.telecom.by:80/announce,http://t.nyaatracker.com:80/announce"
	tracker, err := ioutil.ReadFile(ExePath + "/tracker.txt")
	trackerStr := strings.TrimSpace(string(tracker))
	if err == nil && trackerStr != "" {
		return trackerStr
	}
	return dft
}